import importlib.util
import subprocess
import sys


def package_is_installed(package_name):
    spec = importlib.util.find_spec(package_name)
    if spec is None:
        print(package_name + " is not installed")
        return False
    print(package_name + " is installed")
    return True


def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


def install_custom_package_gitlab(package_name, repo_url, username=None, password=None, branch_name="master"):
    if not package_is_installed(package_name):
        start_index = repo_url.find('//') + 2
        prefix = "git+" + repo_url[:start_index]
        postfix = repo_url[start_index:] + "@" + branch_name + "#egg=" + package_name
        if username and password:
            package = prefix + f"{username}:{password}@" + postfix
        else:
            package = prefix + postfix
        print(f"Installing Package: {package_name}")
        install(package)
    else:
        print(f"Package already installed: {package_name}")