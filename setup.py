from setuptools import setup

setup(
    version='1.0.0',
    name='custom-package-installer',
    description='Helper functions for installing packages from private gitlab repositories',
    author='Chris Toeller',
    py_modules=['custom_packages'],
    url='https://gitlab.com/public-repos5/public-custom-package-installer',
    license='MIT',
    install_requires=[],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries',
        'Programming Language :: Python :: 3.8',
    ],
)
